# This script produces Figure 4 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# In:      simulated_data/Fig4_PAcombinations_BG/BG_scen[1,2,3].csv (define scnaerio in line 24)
# Out:     Figure4_scen[1,2,3].pdf
# Date:    June 11, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import functions.performance_measures as meas
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_theme(style="whitegrid")
import matplotlib
matplotlib.rcParams.update({'font.size': 9})


''' import simulated data '''

scen = 1                                # 1: Exercise in postabsorptive state
                                        # 2: Exercise with insulin bolus reduction
                                        # 3: Exercise without insulin bolus reduction

# exercise start of the scenario to determine acute hypoglycemia
start_scen = [int(6 * 60), int(8.5 * 60), int(9.5 * 60)]
start = start_scen[scen-1]

adj = ['adj0', 'adj1', 'adj2', 'adj3']

# import data for specified scenario for all adjustments and save in list
BG = []
for i in adj:
    tmp = pd.read_csv('simulated_data/Fig4_PAcombinations_BG/BG_scen'+str(scen)+'_'+i+'.csv', sep=';')
    BG.append(tmp)


''' compute TIR, LBGI and HBGI, and acute and late-onset LBGI '''

TIR = pd.DataFrame(data=np.zeros((len(adj), BG[0].shape[1])), columns=BG[0].columns, index=adj)
LBGI = pd.DataFrame(data=np.zeros((len(adj), BG[0].shape[1])), columns=BG[0].columns, index=adj)
HBGI = pd.DataFrame(data=np.zeros((len(adj), BG[0].shape[1])), columns=BG[0].columns, index=adj)
LBGI_acute = pd.DataFrame(data=np.zeros((len(adj), BG[0].shape[1])), columns=BG[0].columns, index=adj)
LBGI_late = pd.DataFrame(data=np.zeros((len(adj), BG[0].shape[1])), columns=BG[0].columns, index=adj)

for i in range(len(adj)):
    for j in range(BG[0].shape[1]):

        # compute TIR, LBGI and HBGI over full simulation
        TIR.loc[adj[i], BG[i].columns[j]] = meas.computeTIR(BG[i][BG[i].columns[j]])
        LBGI.loc[adj[i], BG[i].columns[j]], HBGI.loc[adj[i], BG[i].columns[j]] = meas.computeGI(BG[i][BG[i].columns[j]])

        # compute acute LBGI during exercise and following 60min
        col = BG[i].columns[j]
        _, dur = col.split(', ')
        dur = int(float(dur) * 60)
        BG_acute = BG[i][col].iloc[start:start + dur + 60]
        LBGI_acute.loc[adj[i], col] = meas.computeLBGI(BG_acute.to_list())

        # compute late-onset hypo from 19:00 (t=780) to end of simulation
        BG_late = BG[i][col].iloc[780:]
        LBGI_late.loc[adj[i], col] = meas.computeLBGI(BG_late.to_list())


''' rearrange dataframe for plotting '''

# split into one row for each intensity-duration-adjustment combination,
# and introduce columns 'duration', 'HR', 'TIR', 'LBGI', 'HBGI', 'acute', 'late-onset', 'adjustment'
df_list = []
for column in TIR.columns:
    for index in TIR.index:
        value1 = TIR.loc[index,column]
        value2 = LBGI.loc[index,column]
        value3 = HBGI.loc[index, column]
        value4 = LBGI_acute.loc[index, column]
        value5 = LBGI_late.loc[index, column]
        hr, dur = column.split(',')
        sdf = pd.DataFrame(data=[[dur, hr, value1, value2, value3, value4, value5, index]],
                           columns=['duration',
                                    'HR',
                                    'TIR',
                                    'LBGI',
                                    'HBGI',
                                    'acute',
                                    'late',
                                    'adjustment'])
        df_list.append(sdf)
df = pd.concat(df_list)
df.reset_index(inplace=True, drop=True)
df['HR, Adj'] = df['HR'] + ', ' + df['adjustment']


# rearrange by (HR, adjustment) x duration
dur = ['30', '45', '60', '75', '90', '105', '120']
dfTIR = pd.DataFrame(data=np.zeros((12, 7)), columns=dur,
                     index=df['HR, Adj'].unique())
dfLBGI = pd.DataFrame(data=np.zeros((12, 7)), columns=dur,
                     index=df['HR, Adj'].unique())
dfHBGI = pd.DataFrame(data=np.zeros((12, 7)), columns=dur,
                     index=df['HR, Adj'].unique())
dfacute = pd.DataFrame(data=np.zeros((12, 7)), columns=dur,
                     index=df['HR, Adj'].unique())
dflate = pd.DataFrame(data=np.zeros((12, 7)), columns=dur,
                     index=df['HR, Adj'].unique())

for index in dfTIR.index:
    list_values = df.loc[df['HR, Adj'] == index, :]['TIR'].to_list()
    dfTIR.loc[index] = list_values

    list_values = df.loc[df['HR, Adj'] == index, :]['LBGI'].to_list()
    dfLBGI.loc[index] = list_values

    list_values = df.loc[df['HR, Adj'] == index, :]['HBGI'].to_list()
    dfHBGI.loc[index] = list_values

    list_values = df.loc[df['HR, Adj'] == index, :]['acute'].to_list()
    dfacute.loc[index] = list_values

    list_values = df.loc[df['HR, Adj'] == index, :]['late'].to_list()
    dflate.loc[index] = list_values


''' plot heatmaps for TIR, LBGI and HBGI '''

fig, ax = plt.subplots(1, 5, sharey=True, figsize=(9, 4))

sns.heatmap(dfTIR, linewidths=.5, cmap='plasma_r', ax=ax[0],
            vmin=48, vmax=92)
ax[0].set_xticklabels(ax[0].get_xticklabels(), rotation=90)
ax[0].set_xlabel('')
ax[0].set_title('TIR')

sns.heatmap(dfLBGI, linewidths=.5, cmap='plasma_r', ax=ax[1],
            vmin=0, vmax=12.2)
ax[1].set_xticklabels(ax[1].get_xticklabels(), rotation=90)
ax[1].set_xlabel('')
ax[1].set_title('LBGI')

sns.heatmap(dfHBGI, linewidths=.5, cmap='plasma_r', ax=ax[2],
            vmin=1.4, vmax=3.9)
ax[2].set_xticklabels(ax[2].get_xticklabels(), rotation=90)
ax[2].set_xlabel('')
ax[2].set_title('HBGI')

sns.heatmap(dfacute, linewidths=.5, cmap='plasma_r', ax=ax[3],
            vmin=0, vmax=15)
ax[3].set_xticklabels(ax[3].get_xticklabels(), rotation=90)
ax[3].set_xlabel('')
ax[3].set_title('acute LBGI')

sns.heatmap(dflate, linewidths=.5, cmap='plasma_r', ax=ax[4],
            vmin=0, vmax=15)
ax[4].set_xticklabels(ax[4].get_xticklabels(), rotation=90)
ax[4].set_xlabel('')
ax[4].set_title('late-onset LBGI')

plt.tight_layout()
plt.savefig('Figure4_scen' + str(scen) + '.pdf')
plt.show()
