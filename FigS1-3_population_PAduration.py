# This script produces Supplementary Figures S1-S3 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# In:      simulated_data/Fig5_population_output/[TIR,LBGI,HBGI,acuteLBGI,lateLBGI]_scen[1,2,3].csv
# Out:     FigureS[1,2,3].pdf
# Date:    July 2, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('default')
import seaborn as sns
import matplotlib
matplotlib.rcParams.update({'font.size': 9})
fs = 9


''' define scenario '''

scen = 1                                # 1: Exercise in postabsorptive state
                                        # 2: Exercise with insulin bolus reduction
                                        # 3: Exercise without insulin bolus reduction


''' import simulated data '''

TIR = pd.read_csv('simulated_data/Fig5_population_output/TIR_scen' + str(scen) + '.csv', sep=';').iloc[:, 28:56]
LBGI = pd.read_csv('simulated_data/Fig5_population_output/LBGI_scen' + str(scen) + '.csv', sep=';').iloc[:, 28:56]
HBGI = pd.read_csv('simulated_data/Fig5_population_output/HBGI_scen' + str(scen) + '.csv', sep=';').iloc[:, 28:56]
acuteLBGI = pd.read_csv('simulated_data/Fig5_population_output/acuteLBGI_scen' + str(scen) + '.csv', sep=';').iloc[:, 28:56]
lateLBGI = pd.read_csv('simulated_data/Fig5_population_output/lateLBGI_scen' + str(scen) + '.csv', sep=';').iloc[:, 28:56]


''' plot distribution of TIR, LBGI and HBGI '''

dur = ['30min', '45min', '60min', '75min', '90min', '105min', '120min']
colors = [plt.cm.PuBu(0.8), plt.cm.inferno(0.6), plt.cm.viridis(0.62), plt.cm.viridis(0.1)]
lw = 1.5

fig, ax = plt.subplots(7, 5, figsize=(7.08, 9))

ax[0, 0].set_title('TIR', fontsize=fs)
ax[0, 1].set_title('LBGI', fontsize=fs)
ax[0, 2].set_title('HBGI', fontsize=fs)
ax[0, 3].set_title('acute\nhypo. risk', fontsize=fs)
ax[0, 4].set_title('late-onset\nhypo. risk', fontsize=fs)

for i in range(7):
    sns.kdeplot(data=TIR.iloc[:, i*4:(i+1)*4], palette=colors, linewidth=lw, legend=False,
                ax=ax[i, 0], common_norm=False)

    sns.kdeplot(data=LBGI.iloc[:, i*4:(i+1)*4], palette=colors, linewidth=lw, legend=False,
                ax=ax[i, 1], common_norm=False)

    sns.kdeplot(data=HBGI.iloc[:, i*4:(i+1)*4], palette=colors, linewidth=lw, legend=False,
                ax=ax[i, 2], common_norm=False)

    sns.kdeplot(data=acuteLBGI.iloc[:, i*4:(i+1)*4], palette=colors, linewidth=lw, legend=False,
                ax=ax[i, 3], common_norm=False)

    sns.kdeplot(data=lateLBGI.iloc[:, i*4:(i+1)*4], palette=colors, linewidth=lw, legend=False,
                ax=ax[i, 4], common_norm=False)

for j in range(7):
    ax[j, 1].set_ylim(0, 1.3)
    ax[j, 3].set_ylim(0, 1.52)
    ax[j, 4].set_ylim(0, 0.9)

for i in range(5):
    ax[0, i].set_xticklabels([])
    ax[1, i].set_xticklabels([])

    ymax = []
    xmin = []
    xmax = []
    for j in range(7):
        ymax = ymax + [ax[j, i].get_ylim()[1]]
        xmin = xmin + [ax[j, i].get_xlim()[0]]
        xmax = xmax + [ax[j, i].get_xlim()[1]]
    ymax = max(ymax)
    xmin = min(xmin)
    xmax = max(xmax)

    for j in range(7):
        ax[j, i].set_ylabel('')
        ax[j, i].set_yticks([])

        ax[j, i].set_ylim(0, ymax)
        ax[j, i].set_xlim(xmin, xmax)
        if (i==3) & (xmax>21):
            ax[j, i].set_xlim(xmin, 21)

for j in range(7):
    ax[j, 0].set_ylabel(dur[j])

plt.tight_layout()
plt.savefig('FigureS' + str(scen) + '.pdf')
plt.show()
