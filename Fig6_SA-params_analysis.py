# This script produces Figure 6 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# In:      simulated_data/Fig6_SA-params_output/[TIR,LBGI]_SAparams.csv
# Out:     Figure6[a,b,c,d].pdf
# Date:    June 11, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import functions.SA_interaction_plot as plotS2
from SALib.analyze import sobol
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import matplotlib
matplotlib.rcParams.update({'font.size': 9.5})


''' SALib problem definition '''

# import parameters
params_nom = np.genfromtxt('functions/model_parameters.csv').tolist()
param_names = ['p$_1$', 'p$_2$', 'p$_3$', 'G$_b$', 'V$_g$', 'k$_{21}$', 'k$_d$', 'k$_a$', 'k$_e$', 'V$_i$', r'$\alpha$',
               r'$\beta$', r'$\tau_{HR}$', r'$\tau$', 'a', 'n']

# define parameter bounds
sd_factor = 0.2         # parameter range: +- 20%
bounds = []
for i in range(len(params_nom)):
    tmp = params_nom[i]
    tmp_b = [tmp - abs(sd_factor*tmp), tmp + abs(sd_factor*tmp)]
    bounds.append(tmp_b)

# define SALib problem
problem = {
    'num_vars': len(param_names),
    'names': param_names,
    'bounds': bounds
}


''' compute sensitivity indices '''

TIR = np.genfromtxt('simulated_data/Fig6_SA-params_output/TIR_SAparams.csv', delimiter=',')
LBGI = np.genfromtxt('simulated_data/Fig6_SA-params_output/LBGI_SAparams.csv', delimiter=',')

Si_TIR = sobol.analyze(problem, TIR, calc_second_order=True)
Si_LBGI = sobol.analyze(problem, LBGI, calc_second_order=True)


''' plot main and total effect indices '''

# create dataframe with S1 and ST for TIR
Si_filter = {k:Si_TIR[k] for k in ['ST','ST_conf','S1','S1_conf']}
dfSi_TIR = pd.DataFrame(Si_filter, index=problem['names'])
dfSi_TIR = dfSi_TIR.rename(columns={"ST": "S$_T$", "S1": "S$_1$"})

# create dataframe with S1 and ST for LBGI
Si_filter = {k:Si_LBGI[k] for k in ['ST','ST_conf','S1','S1_conf']}
dfSi_LBGI = pd.DataFrame(Si_filter, index=problem['names'])
dfSi_LBGI = dfSi_LBGI.rename(columns={"ST": "S$_T$", "S1": "S$_1$"})

# plot
colors = [plt.cm.PuBu(0.8), plt.cm.inferno(0.6)]

fig, ax = plt.subplots(1)
indices = dfSi_TIR[['S$_1$','S$_T$']]
err = dfSi_TIR[['S1_conf','ST_conf']]
indices.plot.bar(yerr=err.values.T,ax=ax, color=colors, width=0.7)
ax.set_ylabel('Sobol index TIR')
ax.grid()
fig.set_size_inches(4, 3)
plt.tight_layout()
plt.savefig('Figure6a.pdf')
plt.show()

fig, ax = plt.subplots(1)
indices = dfSi_LBGI[['S$_1$','S$_T$']]
err = dfSi_LBGI[['S1_conf','ST_conf']]
indices.plot.bar(yerr=err.values.T,ax=ax, color=colors, width=0.7)
ax.set_yticks([0, 0.1, 0.2, 0.3])
ax.set_ylabel('Sobol index LBGI')
ax.grid()
fig.set_size_inches(4, 3)
plt.tight_layout()
plt.savefig('Figure6c.pdf')
plt.show()


''' plot 2nd-order Sobol indices '''

sns.set_style('whitegrid')
fig = plotS2.plot_sobol_indices(Si_TIR, problem, criterion='ST', threshold=0.05)
fig.set_size_inches(3, 3)
fig.suptitle('TIR')
plt.tight_layout()
plt.savefig('Figure6b.pdf')
plt.show()

fig = plotS2.plot_sobol_indices(Si_LBGI, problem, criterion='ST', threshold=0.05)
fig.set_size_inches(3, 3)
fig.suptitle('LBGI')
plt.tight_layout()
plt.savefig('Figure6d.pdf')
plt.show()
