# This script produces Figure 1 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# Out:     Figure1.pdf
# Date:    June 11, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import functions.Simulator as SIM
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 9.5})


''' run simulations '''

Gt = 120                # target glucose [mg/dl]
Ib = 10                 # basal insulin [muU/ml]
ICR = 15                # insulin-to-carb ratio
CF = 20                 # correction factor
HRb = 80                # basal heart rate
w = 70                  # weight [kg]
dur = 24                # simulation duration [h]
start = 6               # simulation start [h]

PA_start = 9            # exercise start [h]  (i.e. 9 hours after simulation start = 15:00)
PA_dur = 1              # exercise duration [h]
PA_int = 120            # exercise intensity, heart rate

factor = [0.7, 1, 1.3]  # parameter scaling factors
no_sim = len(factor)

G_all = []              # initialize list for resulting glucose curves

# run simulation
# k = 0: insulin sensitivity is varied (parameter p3)
# k = 1: meal appearance rate is varied (tau_max)
for k in range(2):

    # initialize array to save glucose time courses for 3 parameter values
    G = np.zeros((dur*60, no_sim))

    for i in range(no_sim):
        sim = SIM.Simulator(Gt, Ib, ICR, CF, HRb, w, dur, start, adjustment=0)

        # define time of insulin input [h]
        sim.t_u = [0.75, 6.75, 12.75]

        # define meal input (time t_meal [h], CHO amount d_meal [g], time of max. appearance tau_m [min])
        sim.t_meal = [1, 7, 13]
        sim.d_meal = [50, 70, 60]
        if k == 0:
            sim.tau_m = [60, 60, 60]
        else:
            sim.tau_m = [j * factor[i] for j in [60, 60, 60]]

        # define exercise input
        sim.t_PA = [PA_start, PA_start + PA_dur]        # [start, end]
        sim.d_PA = [PA_int]

        sim.create_input()

        sim.get_parameters()
        if k == 0:
            sim.params[2] = factor[i] * sim.params[2]

        if (sim.adjusted == 2) or (sim.adjusted == 3):
            sim.determine_ins_cond()
            sim.meal_bolus_reduction()
            sim.CHO_during_PA()

        sim.compute_model()

        G[:, i] = sim.model['G']

    G_all.append(G)


''' plot glucose curves '''

label = ['-30%', 'nominal', '+30%']
xtick_pos = np.arange(0, 1440, 360)
xtick_lab = [start + 6*i for i in range(4)]
xtick_lab = [str(i-24)+':00' if i>=24 else str(i)+':00' for i in xtick_lab]
c = plt.cm.ocean(np.linspace(0, 0.8, no_sim))

fig, ax = plt.subplots(1, 2, figsize=(7, 2.2), sharey=True)

for i in range(no_sim):
    ax[0].plot(sim.t, G_all[0][:, i], color=c[i], linewidth=2, label=label[i])
    ax[1].plot(sim.t, G_all[1][:, i], color=c[i], linewidth=2, label=label[i])
for i in range(2):
    ax[i].fill_between([0, 1440], 180, 290, color='grey', alpha=0.2)
    ax[i].fill_between([0, 1440], 20, 70, color='grey', alpha=0.2)
    ax[i].vlines([PA_start*60, (PA_start+PA_dur)*60], 20, 290, linewidth=1)
    ax[i].set_xticks(xtick_pos)
    ax[i].set_xticklabels(xtick_lab)
    ax[i].set_xlim(0, 1440)
    ax[i].grid()

ax[0].set_ylabel('glucose [mg/dl]')
ax[0].set_ylim(50, 290)
ax[1].legend(bbox_to_anchor=(0.62, 0.58))

fig.tight_layout()
plt.savefig('Figure1.pdf')
plt.show()
