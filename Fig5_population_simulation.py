# This script produces the data for Figure 5 and Supplementary Figures S1-S6 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# Out:     simulated_data/Fig5_population_output/[TIR,LBGI,HBGI,acuteLBGI,lateLBGI]_scen[1,2,3].csv
# Date:    July 2, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import functions.performance_measures as meas
import functions.Simulator as SIM
import numpy as np
import pandas as pd


pop_size = 100                  # population size


''' define scenarios '''

# Scenario 1: Exercise in near-fasted state
scen = 1
t_u = [0.75, 8.25, 12.75]       # time of meal bolus injections [h]
t_meal = [1, 8.5, 13]           # time of meal intake [h]
PA_start = 6                    # exercise start [h]

# Scenario 2: Exercise with insulin bolus reduction
# scen = 2
# t_u = [0.75, 6.75, 12.75]
# t_meal = [1, 7, 13]
# PA_start = 8.5

# Scenario 3: Exercise without insulin bolus reduction
# scen = 3
# t_u = [0.75, 6.75, 12.75]
# t_meal = [1, 7, 13]
# PA_start = 9.5


''' sample population parameters '''

params_nom = np.genfromtxt('functions/model_parameters.csv').tolist()

params_pop = np.repeat(np.array(params_nom).reshape(1, 16), pop_size, axis=0)
np.random.seed(seed=100)
f = np.random.normal(1, 0.2, size=(pop_size, 4))

params_pop[:, 0] = params_pop[:, 0] * f[:, 0]
params_pop[:, 2] = params_pop[:, 2] * f[:, 1]
params_pop[:, 10] = params_pop[:, 10] * f[:, 2]
params_pop[:, 11] = params_pop[:, 11] * f[:, 3]


''' define intensities and durations of exercise sessions '''

intensity = [120, 140, 160]
duration = [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]
adj = ['no adj.', 'CHO alg.', 'low CHO', 'high CHO']


''' run simulations '''

Gt = 120
Ib = 10
ICR = 15
CF = 20
HRb = 80
w = 70
dur = 24
start = 6

TIR = pd.DataFrame(data={})
LBGI = pd.DataFrame(data={})
HBGI = pd.DataFrame(data={})
acuteLBGI = pd.DataFrame(data={})
lateLBGI = pd.DataFrame(data={})

for k in range(len(intensity)):
    print('########## HR = ' + str(intensity[k]) + ' ##########')
    for d in range(len(duration)):
        for j in range(4):

            TIRtmp = np.zeros(pop_size)
            LBGItmp = np.zeros(pop_size)
            HBGItmp = np.zeros(pop_size)
            acuteLBGItmp = np.zeros(pop_size)
            lateLBGItmp = np.zeros(pop_size)

            for i in range(pop_size):
                print(str(i) + '/' + str(pop_size) + ' (dur=' + str(duration[d]) + 'h, adj=' + str(j) + ')')

                sim = SIM.Simulator(Gt, Ib, ICR, CF, HRb, w, dur, start, adjustment=j)

                # define insulin input
                sim.t_u = t_u

                # define meal input
                sim.t_meal = t_meal
                sim.d_meal = [50, 70, 60]
                sim.tau_m = [60, 60, 60]

                # define PA input
                sim.t_PA = [PA_start, PA_start + duration[d]]
                sim.d_PA = [intensity[k]]

                sim.create_input()
                sim.params = params_pop[i, :]                   # set parameters
                if (sim.adjusted == 2) or (sim.adjusted == 3):
                    sim.determine_ins_cond()
                    sim.meal_bolus_reduction()
                    sim.CHO_during_PA()
                sim.compute_model()

                G = sim.model['G']
                G_acute = sim.model['G'].iloc[int(sim.t_PA[0] * 60):int((sim.t_PA[1] + 1) * 60)]
                G_late = sim.model['G'].iloc[780:]

                TIRtmp[i] = meas.computeTIR(G)
                LBGItmp[i], HBGItmp[i] = meas.computeGI(G)
                acuteLBGItmp[i] = meas.computeLBGI(np.array(G_acute))
                lateLBGItmp[i] = meas.computeLBGI(np.array(G_late))

            TIR[adj[j]+', '+str(duration[d])+'h, HR=' + str(intensity[k])] = TIRtmp
            LBGI[adj[j] + ', '+str(duration[d])+'h, HR=' + str(intensity[k])] = LBGItmp
            HBGI[adj[j] + ', '+str(duration[d])+'h, HR=' + str(intensity[k])] = HBGItmp
            acuteLBGI[adj[j] + ', '+str(duration[d])+'h, HR=' + str(intensity[k])] = acuteLBGItmp
            lateLBGI[adj[j] + ', '+str(duration[d])+'h, HR=' + str(intensity[k])] = lateLBGItmp


''' compute TIR summary statistics '''

TIRmedian = np.median(TIR, axis=0)
Q1 = np.quantile(TIR, 0.25, axis=0)
Q3 = np.quantile(TIR, 0.75, axis=0)
IQR = Q3 - Q1
CV = np.std(TIR, axis=0) / np.mean(TIR, axis=0) * 100

TIR_summary = pd.DataFrame(data={'median': TIRmedian,
                                 'Q1': Q1,
                                 'Q3': Q3,
                                 'IQR': IQR,
                                 'CV': CV})


''' save TIR, (acute/late) LBGI and HBGI '''

# TIR.to_csv('simulated_data/Fig5_population_output/TIR_scen' + str(scen) + '.csv', sep=';', index=False)
# LBGI.to_csv('simulated_data/Fig5_population_output/LBGI_scen' + str(scen) + '.csv', sep=';', index=False)
# HBGI.to_csv('simulated_data/Fig5_population_output/HBGI_scen' + str(scen) + '.csv', sep=';', index=False)
# acuteLBGI.to_csv('simulated_data/Fig5_population_output/acuteLBGI_scen' + str(scen) + '.csv', sep=';', index=False)
# lateLBGI.to_csv('simulated_data/Fig5_population_output/lateLBGI_scen' + str(scen) + '.csv', sep=';', index=False)
#
# TIR_summary.to_csv('simulated_data/Fig5_population_output/TIR_summary_scen' + str(scen) + '.csv', sep=';')
