# This script produces Figure 5 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# In:      simulated_data/Fig5_population_output/[TIR,LBGI,HBGI,acuteLBGI,lateLBGI]_scen[1,2,3].csv
# Out:     Figure5.pdf
# Date:    July 2, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('default')
import seaborn as sns
import matplotlib
matplotlib.rcParams.update({'font.size': 9})
fs = 9


''' import simulated data '''
# HR = 140, dur = 90min

TIR_s1 = pd.read_csv('simulated_data/Fig5_population_output/TIR_scen1.csv', sep=';').iloc[:, 44:48]
LBGI_s1 = pd.read_csv('simulated_data/Fig5_population_output/LBGI_scen1.csv', sep=';').iloc[:, 44:48]
HBGI_s1 = pd.read_csv('simulated_data/Fig5_population_output/HBGI_scen1.csv', sep=';').iloc[:, 44:48]
acuteLBGI_s1 = pd.read_csv('simulated_data/Fig5_population_output/acuteLBGI_scen1.csv', sep=';').iloc[:, 44:48]
lateLBGI_s1 = pd.read_csv('simulated_data/Fig5_population_output/lateLBGI_scen1.csv', sep=';').iloc[:, 44:48]

TIR_s2 = pd.read_csv('simulated_data/Fig5_population_output/TIR_scen2.csv', sep=';').iloc[:, 44:48]
LBGI_s2 = pd.read_csv('simulated_data/Fig5_population_output/LBGI_scen2.csv', sep=';').iloc[:, 44:48]
HBGI_s2 = pd.read_csv('simulated_data/Fig5_population_output/HBGI_scen2.csv', sep=';').iloc[:, 44:48]
acuteLBGI_s2 = pd.read_csv('simulated_data/Fig5_population_output/acuteLBGI_scen2.csv', sep=';').iloc[:, 44:48]
lateLBGI_s2 = pd.read_csv('simulated_data/Fig5_population_output/lateLBGI_scen2.csv', sep=';').iloc[:, 44:48]

TIR_s3 = pd.read_csv('simulated_data/Fig5_population_output/TIR_scen3.csv', sep=';').iloc[:, 44:48]
LBGI_s3 = pd.read_csv('simulated_data/Fig5_population_output/LBGI_scen3.csv', sep=';').iloc[:, 44:48]
HBGI_s3 = pd.read_csv('simulated_data/Fig5_population_output/HBGI_scen3.csv', sep=';').iloc[:, 44:48]
acuteLBGI_s3 = pd.read_csv('simulated_data/Fig5_population_output/acuteLBGI_scen3.csv', sep=';').iloc[:, 44:48]
lateLBGI_s3 = pd.read_csv('simulated_data/Fig5_population_output/lateLBGI_scen3.csv', sep=';').iloc[:, 44:48]


''' plot distribution of TIR, LBGI and HBGI '''

adj = ['no adj.', 'CHO alg.', 'low CHO', 'high CHO']
colors = [plt.cm.PuBu(0.8), plt.cm.inferno(0.6), plt.cm.viridis(0.62), plt.cm.viridis(0.1)]
lw = 1.5

fig, ax = plt.subplots(3, 5, figsize=(7.08, 4.3))

ax[0, 0].set_title('TIR', fontsize=fs)
sns.kdeplot(data=TIR_s1, palette=colors, linewidth=lw, legend=False,
            ax=ax[0, 0], common_norm=False)
sns.kdeplot(data=TIR_s2, palette=colors, linewidth=lw, legend=False,
            ax=ax[1, 0], common_norm=False)
sns.kdeplot(data=TIR_s3, palette=colors, linewidth=lw, legend=False,
            ax=ax[2, 0], common_norm=False)

ax[0, 1].set_title('LBGI', fontsize=fs)
sns.kdeplot(data=LBGI_s1, palette=colors, linewidth=lw, legend=False,
            ax=ax[0, 1], common_norm=False)
sns.kdeplot(data=LBGI_s2, palette=colors, linewidth=lw, legend=False,
            ax=ax[1, 1], common_norm=False)
sns.kdeplot(data=LBGI_s3, palette=colors, linewidth=lw, legend=False,
            ax=ax[2, 1], common_norm=False)

ax[0, 2].set_title('HBGI', fontsize=fs)
sns.kdeplot(data=HBGI_s1, palette=colors, linewidth=lw, legend=False,
            ax=ax[0, 2], common_norm=False)
sns.kdeplot(data=HBGI_s2, palette=colors, linewidth=lw, legend=False,
            ax=ax[1, 2], common_norm=False)
sns.kdeplot(data=HBGI_s3, palette=colors, linewidth=lw, legend=False,
            ax=ax[2, 2], common_norm=False)

ax[0, 3].set_title('acute\nhypo. risk', fontsize=fs)
sns.kdeplot(data=acuteLBGI_s1, palette=colors, linewidth=lw, legend=False,
            ax=ax[0, 3], common_norm=False)
sns.kdeplot(data=acuteLBGI_s2, palette=colors, linewidth=lw, legend=False,
            ax=ax[1, 3], common_norm=False)
sns.kdeplot(data=acuteLBGI_s3, palette=colors, linewidth=lw, legend=False,
            ax=ax[2, 3], common_norm=False)

ax[0, 4].set_title('late-onset\nhypo. risk', fontsize=fs)
sns.kdeplot(data=lateLBGI_s1, palette=colors, linewidth=lw, legend=False,
            ax=ax[0, 4], common_norm=False)
sns.kdeplot(data=lateLBGI_s2, palette=colors, linewidth=lw, legend=False,
            ax=ax[1, 4], common_norm=False)
sns.kdeplot(data=lateLBGI_s3, palette=colors, linewidth=lw, legend=False,
            ax=ax[2, 4], common_norm=False)

for i in range(3):
    ax[i, 3].set_ylim(0, 1.52)

for i in range(5):
    ax[0, i].set_xticklabels([])
    ax[1, i].set_xticklabels([])

    ymax = max(ax[0, i].get_ylim()[1], ax[1, i].get_ylim()[1], ax[2, i].get_ylim()[1])
    xmin = min(ax[0, i].get_xlim()[0], ax[1, i].get_xlim()[0], ax[2, i].get_xlim()[0])
    xmax = max(ax[0, i].get_xlim()[1], ax[1, i].get_xlim()[1])

    for j in range(3):
        ax[j, i].set_ylabel('')
        ax[j, i].set_yticks([])

        ax[j, i].set_ylim(0, ymax)
        ax[j, i].set_xlim(xmin, xmax)
        if (i==3) & (xmax>21):
            ax[j, i].set_xlim(xmin, 21)

plt.tight_layout()
plt.savefig('Figure5.pdf')
plt.show()
