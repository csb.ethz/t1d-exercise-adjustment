# This script produces Figure 2 & 3 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# Out:     Figure2.pdf, Figure3.pdf
# Date:    June 11, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import functions.Simulator as SIM
import functions.performance_measures as meas
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 9.5})


''' run simulations '''

Gt = 120                # target glucose [mg/dl]
Ib = 10                 # basal insulin [muU/ml]
ICR = 15                # insulin-to-carb ratio
CF = 20                 # correction factor
HRb = 80                # basal heart rate
w = 70                  # weight [kg]
dur = 24                # simulation duration [h]
start = 6               # simulation start [h]

adj = [0, 1, 2, 3]      # list of adjustments
                        # 0: no adjustment; 1: CHO algorithm
                        # 2: consensus guidelines (low CHO); 3: consensus guidelines (high CHO)

# list of exercise start times (between 13:30 and 17:30 in 30min steps)
PA_start = [7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5]
PA_dur = 1              # exercise duration [h]
PA_int = 120            # exercise intensity, heart rate

no_sim = len(PA_start)  # number of simulations per treatment adjustment
G_all = []              # initialize list for resulting glucose curves

# run simulations
for k in adj:

    # glucose for each starting time and adjustment k
    G = np.zeros((dur*60, no_sim))

    for i in range(no_sim):
        sim = SIM.Simulator(Gt, Ib, ICR, CF, HRb, w, dur, start, adjustment=k)

        # define time of insulin input
        sim.t_u = [0.75, 6.75, 12.75]

        # define meal input
        sim.t_meal = [1, 7, 13]
        sim.d_meal = [50, 70, 60]
        sim.tau_m = [60, 60, 60]

        # define PA input
        sim.t_PA = [PA_start[i], PA_start[i] + PA_dur]
        sim.d_PA = [PA_int]

        sim.run_simulation(plot=False)

        G[:, i] = sim.model['G']

    G_all.append(G)


''' plot glucose curves '''

xtick_pos = np.arange(0, 1440, 360)
xtick_lab = [start + 6*i for i in range(4)]
xtick_lab = [str(i-24)+':00' if i>=24 else str(i)+':00' for i in xtick_lab]

ytick = [50, 100, 150, 200, 250]

labels = ['13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30']

c = plt.cm.ocean(np.linspace(0, 0.85, no_sim))


fig, ax = plt.subplots(2, 2, figsize=(7, 4.3), sharey=True, sharex=True)

for i in range(no_sim):
    ax[0, 0].plot(sim.t, G_all[0][:, i], color=c[i], linewidth=1.5, label=labels[i])
    ax[0, 1].plot(sim.t, G_all[1][:, i], color=c[i], linewidth=1.5)
    ax[1, 0].plot(sim.t, G_all[2][:, i], color=c[i], linewidth=1.5)
    ax[1, 1].plot(sim.t, G_all[3][:, i], color=c[i], linewidth=1.5)
for i in range(2):
    for j in range(2):
        ax[i, j].fill_between([0, 1440], 180, 290, color='grey', alpha=0.2)
        ax[i, j].fill_between([0, 1440], 20, 70, color='grey', alpha=0.2)
        ax[i, j].grid()

ax[0, 0].set_ylabel('glucose [mg/dl]')
ax[0, 0].set_ylim(40, 260)
ax[0, 0].set_yticks(ytick)
ax[0, 0].set_yticklabels([str(i) for i in ytick])

ax[1, 0].set_ylabel('glucose [mg/dl]')
ax[1, 0].set_xticks(xtick_pos)
ax[1, 0].set_xticklabels(xtick_lab)
ax[1, 0].set_xlim(0, 1440)

# ax[0, 0].legend()

fig.tight_layout()
plt.savefig('Figure2.pdf')
plt.show()


''' compute TIR and LBGI '''

TIR = np.zeros((no_sim, len(adj)))
LBGI = np.zeros((no_sim, len(adj)))
for k in adj:
    for i in range(no_sim):
        TIR[i, k] = meas.computeTIR(G_all[k][:, i])
        LBGI[i, k] = meas.computeLBGI(G_all[k][:, i])


''' plot TIR and LBGI '''

colors = [plt.cm.PuBu(0.8), plt.cm.inferno(0.6), plt.cm.viridis(0.62), plt.cm.viridis(0.1)]
adj_labels=['No adj.', 'CHO alg.', 'low CHO', 'high CHO']

fig, ax = plt.subplots(1, 2, figsize=(7.08, 2.3), sharex=True)
for k in adj:
    ax[0].plot(np.arange(no_sim), TIR[:, k], color=colors[k], linewidth=1.5)
    ax[0].scatter(np.arange(no_sim), TIR[:, k], s=36, color=colors[k], zorder=5)
    ax[1].plot(np.arange(no_sim), LBGI[:, k], color=colors[k], linewidth=1.5)
    ax[1].scatter(np.arange(no_sim), LBGI[:, k], s=36, color=colors[k], label=adj_labels[k], zorder=5)

ax[0].set_xlabel('exercise start')
ax[0].set_xticks([0, 2, 4, 6, 8])
ax[0].set_xticklabels(['13:30', '14:30', '15:30', '16:30', '17:30'])
ax[0].set_ylabel('TIR [%]')
ax[0].grid()

ax[1].set_xlabel('exercise start')
ax[1].set_ylabel('LBGI')
ax[1].legend()
ax[1].grid()

plt.tight_layout()
plt.savefig('Figure3.pdf')
plt.show()
