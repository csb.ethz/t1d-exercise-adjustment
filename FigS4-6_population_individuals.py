# This script produces Supplementary Figures S4-S6 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# In:      simulated_data/Fig5_population_output/[TIR,LBGI,HBGI,acuteLBGI,lateLBGI]_scen[1,2,3].csv
# Out:     FigureS[4,5,6].pdf
# Date:    July 2, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('default')
import matplotlib
matplotlib.rcParams.update({'font.size': 9})
fs = 9


''' define scenario '''

scen = 1                                # 1: Exercise in postabsorptive state
                                        # 2: Exercise with insulin bolus reduction
                                        # 3: Exercise without insulin bolus reduction


''' import simulated data '''

TIR = pd.read_csv('simulated_data/Fig5_population_output/TIR_scen' + str(scen) + '.csv', sep=';').iloc[:10, np.r_[36:40, 44:48]]
LBGI = pd.read_csv('simulated_data/Fig5_population_output/LBGI_scen' + str(scen) + '.csv', sep=';').iloc[:10, np.r_[36:40, 44:48]]
HBGI = pd.read_csv('simulated_data/Fig5_population_output/HBGI_scen' + str(scen) + '.csv', sep=';').iloc[:10, np.r_[36:40, 44:48]]
acuteLBGI = pd.read_csv('simulated_data/Fig5_population_output/acuteLBGI_scen' + str(scen) + '.csv', sep=';').iloc[:10, np.r_[36:40, 44:48]]
lateLBGI = pd.read_csv('simulated_data/Fig5_population_output/lateLBGI_scen' + str(scen) + '.csv', sep=';').iloc[:10, np.r_[36:40, 44:48]]


''' plot TIR, LBGI and HBGI of 10 subjects '''

adj = ['No adj.', 'CHO alg.', 'low CHO', 'high CHO']
colors = [plt.cm.PuBu(0.8), plt.cm.inferno(0.6), plt.cm.viridis(0.62), plt.cm.viridis(0.1)]
markers = ['o', 'p', 'X', 'v']

fig, ax = plt.subplots(2, 5, sharey=True, figsize=(7.08, 5.2))

for i in range(4):
    ax[0, 0].set_title('TIR', fontsize=fs)
    ax[0, 0].scatter(TIR.iloc[:, i], np.arange(10)+1, color=colors[i], marker=markers[i], zorder=5)
    ax[1, 0].scatter(TIR.iloc[:, i + 4], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)

    ax[0, 1].set_title('LBGI', fontsize=fs)
    ax[0, 1].scatter(LBGI.iloc[:, i], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)
    ax[1, 1].scatter(LBGI.iloc[:, i + 4], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)

    ax[0, 2].set_title('HBGI', fontsize=fs)
    ax[0, 2].scatter(HBGI.iloc[:, i], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)
    ax[1, 2].scatter(HBGI.iloc[:, i + 4], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)

    ax[0, 3].set_title('acute\nhypo. risk', fontsize=fs)
    ax[0, 3].scatter(acuteLBGI.iloc[:, i], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)
    ax[1, 3].scatter(acuteLBGI.iloc[:, i + 4], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)

    ax[0, 4].set_title('late-onset\nhypo. risk', fontsize=fs)
    ax[0, 4].scatter(lateLBGI.iloc[:, i], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)
    ax[1, 4].scatter(lateLBGI.iloc[:, i + 4], np.arange(10) + 1, color=colors[i], marker=markers[i], zorder=5)

for i in range(5):
    ax[0, i].set_xticks([])
    xmin = min(ax[0, i].get_xlim()[0], ax[1, i].get_xlim()[0])
    xmax = max(ax[0, i].get_xlim()[1], ax[1, i].get_xlim()[1])
    ax[0, i].set_xlim(xmin, xmax)
    ax[1, i].set_xlim(xmin, xmax)

for i in range(2):
    ax[i, 0].set_yticks(np.arange(10)+1)
    ax[i, 0].set_ylabel('patient no.')
    for j in range(5):
        ax[i, j].grid(axis='y')

plt.tight_layout()
plt.savefig('FigureS' + str(scen+3) + '.pdf')
plt.show()
