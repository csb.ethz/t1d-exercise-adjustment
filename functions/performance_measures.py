import numpy as np

def computeTIR(BG, lowerBG=70, upperBG=180):
    """ Compute time-in-range (TIR).
    :param array-like BG: glucose levels
    :param lowerBG: lower bound of range, default=70mg/dl
    :param upperBG: upper bound of range, default=180mg/dl
    :return: TIR [%]
    """

    dur = len(BG)
    TIR = len(BG[(BG >= lowerBG) & (BG <= upperBG)]) / dur * 100
    return TIR


def computeLBGI(BG):
    """ Compute low blood glucose index (LBGI)
    :param array-like BG: glucose levels [mg/dl]
    :return: LBGI
    """

    dur = len(BG)
    rl = np.zeros(dur)
    for i in range(dur):
        if BG[i] < 112.5:
            rl[i] = 10 * (1.509 * ((np.log(BG[i]))**1.084 - 5.381))**2
    LBGI = np.mean(rl)
    return LBGI


def computeGI(BG):
    """ Compute low and high blood glucose index (LBGI and HBGI)

    :param BG: glucose levels [mg/dl]
    :return: LBGI, HBGI
    """

    dur = len(BG)
    rl = np.zeros(dur)
    rh = np.zeros(dur)
    for i in range(dur):
        if BG[i] < 112.5:
            rl[i] = 10 * (1.509 * ((np.log(BG[i]))**1.084 - 5.381))**2
        elif BG[i] >= 112.5:
            rh[i] = 10 * (1.509 * ((np.log(BG[i]))**1.084 - 5.381))**2
    LBGI = np.mean(rl)
    HBGI = np.mean(rh)
    return LBGI, HBGI
