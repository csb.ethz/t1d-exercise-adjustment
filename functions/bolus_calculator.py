import numpy as np

def bolus_calc(D, BG, BGt, ICR, CF, f):
    """ Compute insulin bolus.
    :param D: CHO amount [g]
    :param BG: current glucose level [mg/dl]
    :param BGt: glucose target [mg/dl]
    :param ICR: insulin-to-carb ratio
    :param CF: correction factor
    :param f: scaling factor
    :return: insulin bolus size
    """
    bolus = D / ICR + (BG - BGt) / CF
    bolus = np.round(2 * f * bolus) / 2 * 1e6      # steps of 0.5U
    bolus = max(bolus, 0)                          # ensure that u >= 0

    return bolus
