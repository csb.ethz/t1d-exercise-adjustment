import functions.GI_Model as GIM
from scipy.integrate import odeint
import numpy as np


# Exercise adjustments based on Riddell et. al, 2017, "Exercise management in type 1 diabetes: a consensus statement"
# for short (up to 120min), moderate-intensity exercise.
#
# Includes functions to determine insulin condition, pre-exercise bolus reduction, CHO intake at start of and during
# exercise, and potential delay of exercise due to low glucose levels


def insulin_condition(u_beforePA):
    """ Compute insulin condition before exercise based on insulin injections from pre-exercise period.
    :param list u_beforePA: insulin input prior to PA (120min)
    :return: insulin condition (=1, if insulin was administered; =0, if no insulin was administered)
    """

    if max(u_beforePA) > 0:
        ins_cond = 1
    else:
        ins_cond = 0
    return ins_cond


def prePAmeal(intensity, duration):
    """ Compute insulin bolus reduction of pre-exercise meal based on intensity and duration of the activity.
    :param float intensity: exercise intensity in heart rate
    :param float duration: exercise duration in minutes
    :return: fraction of standard bolus to be administered
    """

    if (intensity >= 100) & (intensity < 130):          # 25-50%VO2max
        if (duration >= 30) & (duration < 60):
            ins_factor = 0.75
        elif duration >= 60:
            ins_factor = 0.5
        else:
            ins_factor = 1
    elif (intensity >= 130) & (intensity < 155):        # 50-70% VO2max
        if (duration >= 30) & (duration < 60):
            ins_factor = 0.5
        elif duration >= 60:
            ins_factor = 0.25
        else:
            ins_factor = 1
    elif intensity >= 155:                              # 70-75% VO2max
        if duration >= 30:
            ins_factor = 0.25
        else:
            ins_factor = 1
    else:
        ins_factor = 1

    return ins_factor


def startPA(model_PAstart, ins_cond):
    """ Compute CHO intake at start of exercise based on glucose level and insulin condition.
    :param list model_PAstart: model state at the onset of exercise
    :param int ins_cond: insulin condition (low=0, high=1)
    :return CHO intake amount [mg]
    """

    glc = model_PAstart[4]

    # If BG < 90 mg/dl, give CHO
    if glc < 90:
        if ins_cond == 0:
            CHO = 15
        else:
            CHO = 25

    # if 90 <= BG < 124: give glc
    elif (glc >= 90) & (glc < 124):
        CHO = 10

    else:
        CHO = 0

    return CHO * 1e3


def postponePA(model_PAstart, Gt, Ib, u, Ra, HR, HRb, PA_dur, params, weight):
    """ Compute whether PA has to be postponed based on glucose level at exercise onset and update exercise start and
    heart rate accordingly.

    :param list model_PAstart: model state at the onset of exercise
    :param float Gt: target glucose [mg/dl]
    :param float Ib: basal insulin [muU/ml]
    :param list u: insulin input from exercise onset
    :param list Ra: meal appearance rate from exercise onset
    :param list HR: heart rate from exercise onset
    :param float HRb: basal heart rate
    :param int PA_dur: exercise duration [min]
    :param list params: model parameters
    :param float weight: weight [kg]
    :return: PA_break: duration of exercise postponement; HR: updated heart rate
    """

    HR_tmp = HRb * np.ones(len(HR))

    dur = len(u)
    Xt = - params[0] * (Gt - params[3]) / Gt
    dIt = params[1] / params[2] * Xt

    y0 = model_PAstart
    model = np.zeros((dur, len(y0)))
    model[0, :] = y0
    for i in range(dur - 1):
        sol = odeint(GIM.GI_model, model[i, :], [0, 1], args=(params, dIt, Ib, u[i], Ra[i], HR_tmp[i], HRb, 0, weight))
        model[i + 1, :] = sol[1]

    tmp_glc = model[:, 4]
    idx = np.where(tmp_glc >= 90)[0][0]
    HR_copy = HRb * np.ones(len(HR))
    HR_copy[idx:idx + PA_dur] = HR[:PA_dur]
    HR = HR_copy
    PA_break = [idx]

    return PA_break, HR


def duringPA(PA_dur, ins_cond, limit=0):
    """ Compute CHO intake during exercise based on duration and insulin condition.

    :param int PA_dur: exercise duration [min]
    :param int ins_cond: insulin condition (low=0, high=1)
    :param int limit: low or high CHO recommendation (low=0, high=1)
    :return: timing and amount of CHO intake during exercise
    """

    t_CHO = []
    CHO = []

    if PA_dur <= 30:
        if ins_cond == 1:
            # 15-30 g CHO: give at PA half-time
            CHO_range = [15, 30]
            t_CHO = [int(round(PA_dur/2))]
            CHO = [CHO_range[limit]]

    if (PA_dur > 30) & (PA_dur <= 60):
        if ins_cond == 0:
            # 10-15g CHO per hour: give CHO twice (5-8g per 30 min)
            CHO_range = [5, 8]

        else:
            # 15-30g CHO per 30 min
            CHO_range = [15, 30]

        step = int(round(PA_dur / 3))                           # give CHO twice at step and 2*step
        t_CHO = [step, 2 * step]
        CHO = 2 * [round(CHO_range[limit] * PA_dur / 60)]       # amount of CHO based on PA duration

    if PA_dur > 60:
        full_30min = PA_dur // 30                               # how many full 30min intervals
        step = int(round(PA_dur) / (full_30min + 1))            # give CHO for each full 30min at step, 2*step, ...

        for i in range(full_30min):
            t_CHO.append((i+1) * step)

        if ins_cond == 0:
            # 30-60g per hour --> 15-30g per 30min
            CHO_range = [15, 30]

        else:
            # 60-70g per hour --> 30-35g per 30min
            CHO_range = [30, 35]

        CHO = full_30min * [CHO_range[limit]]

    if len(t_CHO) > 0:
        CHO = [i * 1e3 for i in CHO]

    return t_CHO, CHO



# CHO intake during exercise based on Riddell and Milliken, 2011, "Preventing exercise-induced hypoglycemia in type 1
# diabetes using real-time continuous glucose monitoring and a new carbohydrate intake algorithm: An observational
# field study". CHO intake is recommended based on a decision tree considering glucose levels and trends.


def CHO_algorithm(glc):
    """ Compute CHO requirements based on glucose level and glucose trend.
    :param list glc: Glucose levels from previous 5 min [mg/dl]
    :return: recommended CHO amount [mg] and which action from the decision tree is taken
    """

    if glc[-1] < 126:
        if glc[-1] >= 110:
            if (glc[0] - glc[-1]) >= 5.4:
                action = 1
            else:
                action = 0
        if (glc[-1] >= 90) & (glc[-1] < 110):
            if (glc[0] - glc[-1]) > 9.9:
                action = 3
            elif ((glc[0] - glc[-1]) >= 5.4) & ((glc[0] - glc[-1]) <= 9.9):
               action = 2
            else:
                action = 0
        if glc[-1] < 90:
            action = 4
    else:
        action = 0


    if action == 0:
        CHO = 0
    if action == 1:
        CHO = 8e3
    if action == 2:
        CHO = 16e3
    if action == 3:
        CHO = 20e3
    if action == 4:
        CHO = 16e3

    return CHO, action

