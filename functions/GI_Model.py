import numpy as np
import matplotlib.pyplot as plt


def GI_model(y, t, params, dIt, Ib, u, Ra, HR, HRb, HRint, w):
    """ ODE model of glucose-insulin regulation including moderate-intensity exercise.

    :param list y: model state
    :param t: time
    :param list params: model parameters
    :param float dIt: difference between target and basal insulin concentration [muU/ml]
    :param float Ib: basal insulin concentration [muU/ml]
    :param float u: insulin input [muU/min]
    :param float Ra: rate of glucose appearance [mg/min]
    :param float HR: heart rate [bpm]
    :param float HRb: basal heart rate [bpm]
    :param HRint: integrated heart rate
    :param w: weight [kg]
    :return: dydt
    """

    p1, p2, p3, Gb, Vg, k21, kd, ka, ke, Vi, alpha, beta, tau_HR, tau, a, n = params
    x1, x2, Ic, X, G, Y, Z = y

    # difference in insulin concentration to basal
    dI = dIt + Ic
    # basal insulin action
    Xb = p3/p2 * Ib

    # exercise transfer function
    fY = (Y / (a * HRb))**n / (1 + (Y / (a * HRb))**n)

    # model ODE system
    dydt = [u - k21 * x1,
            k21 * x1 - (kd + ka) * x2,
            ka / (Vi * w) * x2 - ke * Ic,
            -p2 * X + p3 * dI,
            -p1 * (G-Gb) - (1+alpha*HRint*Z) * X * G - alpha * HRint * Z * Xb * G - beta * Y * G + Ra / (Vg * w),
            -1 / tau_HR * Y + 1 / tau_HR * (HR-HRb),
            -(fY + 1/tau) * Z + fY]

    return dydt


def Ra_meal(Ra, D, t_meal, tau_m, f=0.8):
    """ Glucose rate of appearance from meal CHOs.

    :param array-like Ra: Array with time course of the current appearance rate (from previous meals)
    :param float D: CHO amount [mg]
    :param float t_meal: time of meal intake [min]
    :param float tau_m: time of maximum glucose appearance [min]
    :param float f: bioavailability
    :return: updated rate of appearance
    """

    dur_tot = len(Ra)
    dur = dur_tot - t_meal
    t = np.arange(dur)

    Ra_tmp = f * D * t * np.exp(-t / tau_m) / tau_m**2

    Ra[t_meal:] = Ra[t_meal:] + Ra_tmp

    return Ra


def plot_glucose(model, start, pa=None):
    """ Plot simulated glucose dynamics. """

    dur = len(model)
    t = np.arange(dur)

    no_xticks = int(dur / 240)
    tick_pos = np.linspace(60, len(model)-180, no_xticks)
    x_ticks = [start + 1 + i * 4 for i in range(no_xticks)]
    x_ticks = [str(i - 24)+':00' if i >= 24 else str(i)+':00' for i in x_ticks]

    fs = 16

    # plot
    fig, axs = plt.subplots(1, 1, figsize=(7, 5), sharex=True)

    axs.plot(t, model['G'], linewidth=2, color=plt.cm.ocean_r(0.85))
    ymin, ymax = axs.get_ylim()
    axs.fill_between([t[0] - 5, t[-1] + 5], 0, 70, alpha=0.2, color='grey')
    axs.fill_between([t[0] - 5, t[-1] + 5], 180, max(200, ymax+5), alpha=0.2, color='grey')
    if pa is not None:
        axs.vlines(pa, min(50, ymin-5), max(200, ymax+5), colors='black')
    axs.set_ylim(min(50, ymin-5), max(200, ymax+5))
    axs.set_xlim(t[0]-5, t[-1]+5)
    axs.grid()
    axs.set_xticks(tick_pos)
    axs.set_xticklabels(x_ticks)
    axs.xaxis.set_tick_params(labelsize=fs)
    axs.yaxis.set_tick_params(labelsize=fs)
    axs.set_ylabel('glucose [mg/dl]', fontsize=fs)

    plt.show()

    return fig, axs
