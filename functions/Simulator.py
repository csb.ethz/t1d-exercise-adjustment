import functions.GI_Model as GIM
import functions.PA_Adjustment as Adj
import functions.bolus_calculator as calc
import numpy as np
import pandas as pd
from scipy.integrate import odeint


class Simulator:

    def __init__(self, Gt, Ib, ICR, CF, HRb, weight, sim_duration, sim_start, adjustment=0):
        """ Simulate glucose dynamics applying different treatment adjustment guidelines to exercise.
        run_simulation carries out actions in correct order and produces figure of resulting glucose trajectory.

        :param float Gt: target glucose [mg/dl]
        :param float Ib: basal insulin [muU/ml]
        :param float ICR: insulin-to-carb ratio
        :param float CF: correction factor
        :param float HRb: basal heart rate [bpm]
        :param float weight: weight [kg]
        :param flaot sim_duration: simulation duration [h]
        :param float sim_start: time of simulation start [h]
        :param int adjustment: exercise adjusmtent strategy (0: no adjustment; 1: CHO algorithm; 2: consensus guidelines
                                (low CHO); 3: consensus guidelines (high CHO))
        """

        self.Gt = Gt
        self.Ib = Ib
        self.ICR = ICR
        self.CF = CF
        self.HRb = HRb
        self.w = weight

        self.dur = int(sim_duration * 60)
        self.t = np.arange(self.dur)
        self.sim_start = sim_start

        # initialize lists for time [h], amount [g], time of max. appearance [min] for meals
        self.t_meal = []
        self.d_meal = []
        self.tau_m = []
        self.tau_m_pa = 20

        # initialize lists for exercise time and provide with start and end time [h], and for intensity (HR)
        self.t_PA = []
        self.d_PA = []

        # initialize list for timing of meal insulin bolus [h]
        self.t_u = []
        self.d_u = []

        self.adjusted = adjustment


    def create_input(self):
        """ Create model input (convert input into minutes and create input arrays) """
        self.HR = self.HRb * np.ones(self.dur)
        self.u = np.zeros(self.dur)
        self.Ra = np.zeros(self.dur)

        self.t_meal = [int(i * 60) for i in self.t_meal]
        self.d_meal = [i * 1e3 for i in self.d_meal]

        self.t_u = [int(i * 60) for i in self.t_u]

        t_PA = [int(i * 60) for i in self.t_PA]
        if len(t_PA) > 0:
            self.HR[t_PA[0]:t_PA[1]] = self.d_PA

            self.PA_start = t_PA[0]
            self.PA_start0 = self.PA_start
            self.PA_end = t_PA[1]
            self.PA_dur = t_PA[1] - t_PA[0]

            self.PA_break = 0
            self.t_meal_PA = []
            self.d_meal_PA = []
            self.action = []


    def get_parameters(self):
        # read in parameters
        self.params = np.genfromtxt('functions/model_parameters.csv', delimiter=',').tolist()


    def determine_ins_cond(self):
        # determine insulin condition for consensus guidelines
        u_tmp = self.u[max(0, self.PA_start - 120):self.PA_start]
        self.ins_cond = Adj.insulin_condition(u_tmp)

    def meal_bolus_reduction(self):
        # determine factor to reduce insulin bolus for consensus guidelines
        self.bolus_red = Adj.prePAmeal(self.d_PA[0], self.PA_dur)

    def CHO_during_PA(self):
        # determine CHO intake during exercise for consensus guidelines
        if self.adjusted == 2:
            limit = 0
        elif self.adjusted == 3:
            limit = 1
        self.t_meal_PA, self.d_meal_PA = Adj.duringPA(self.PA_dur, self.ins_cond, limit)


    def compute_Ra(self, t, D, tau):
        # determine glucose appearance rate from meals
        for i in t:
            idx = t.index(i)
            self.Ra = GIM.Ra_meal(self.Ra, D[idx], i, tau[idx])


    def compute_model(self):
        """ Compute model output over simulation duration. """

        # rate of appearance of pre-planned meals
        self.compute_Ra(self.t_meal, self.d_meal, self.tau_m)

        p1, p2, p3, Gb, Vg, k21, kd, ka, ke, Vi, alpha, beta, tau_HR, tau, a, n = self.params

        # determine target insulin action (Xt) and difference in concentration to baseline (dIt)
        Xt = - p1 * (self.Gt - Gb) / self.Gt
        dIt = p2 / p3 * Xt

        # initialize integrated intensity
        HRint = 0

        # initialize model
        y0 = [0, 0, 0, Xt, self.Gt, 0, 0]
        model = np.zeros((self.dur, len(y0)))
        model[0, :] = y0

        for i in range(self.dur - 1):

            # compute insulin bolus
            if i in self.t_u:
                # extract size of corresponding meal
                idx = np.argmin(np.abs(i - np.array(self.t_meal)))
                d = self.d_meal[idx] * 1e-3
                f = 1

                # reduction factor for insulin bolus for consensus guidelines
                if (self.adjusted == 2) or (self.adjusted == 3):
                    if (self.PA_start - i <= 120) & (self.PA_start - i >= 0):
                        f = self.bolus_red

                # compute the insulin bolus
                bolus = calc.bolus_calc(d, model[i, 4], self.Gt, self.ICR, self.CF, f)
                self.u[i] = bolus
                self.d_u.append(bolus)

            # determine exercise adjustments

            # CHO intake algorithm
            if self.adjusted == 1:
                if (i >= self.PA_start) & (i <= self.PA_end):

                    # compute CHO intake based on glucose levels from previous 5 min (min. 20min between CHO intake)
                    if (len(self.t_meal_PA) == 0) or (min([i-j for j in self.t_meal_PA]) >= 20):
                        CHO, act = Adj.CHO_algorithm(model[i-4:i+1, 4])
                        if CHO > 0:
                            self.action.append(act), self.d_meal_PA.append(CHO), self.t_meal_PA.append(i)
                            # update rate of appearance
                            self.compute_Ra([i], [CHO], [self.tau_m_pa])

                    # suspend exercise if glucose is below 70mg/dl
                    if model[i, 4] < 70:
                        self.HR[i] = self.HRb
                        self.PA_break += 1

            # consensus guidelines
            if (self.adjusted == 2) or (self.adjusted == 3):

                if i == self.PA_start0:
                    # compute CHO requirements at start of exercise based on BG
                    CHO = Adj.startPA(model[i, :], self.ins_cond)
                    if CHO > 0:
                        self.t_meal.append(i), self.d_meal.append(CHO), self.tau_m.append(self.tau_m_pa)
                        # update rate of apearance
                        self.compute_Ra([i], [CHO], [self.tau_m_pa])

                    # if starting glucose < 90, postpone PA
                    if model[i, 4] < 90:
                        self.PA_break, self.HR[i:] = Adj.postponePA(model[i, :], self.Gt, self.Ib, self.u[i:],
                                                                    self.Ra[i:], self.HR[i:], self.HRb, self.PA_dur,
                                                                    self.params, self.w)
                        self.PA_start = self.PA_start + self.PA_break[0]

                    # update meals and appearance rate for meals taken in during exercise based on actual starting time
                    self.t_meal_PA = [j + self.PA_start for j in self.t_meal_PA]
                    self.compute_Ra(self.t_meal_PA, self.d_meal_PA, [self.tau_m_pa] * len(self.d_meal_PA))

                # ingestion of 20g CHO if glucose at end of exercise is < 90mg/dl
                if (i == self.PA_start + self.PA_dur) & (model[i, 4] < 90):
                    self.t_meal_PA.append(i)
                    self.d_meal_PA.append(20e3)
                    # update rate of appearance
                    self.compute_Ra([i], [20e3], [self.tau_m_pa])

            # update integrated heart rate
            HRint += self.HR[i] - self.HRb

            # compute model step
            sol = odeint(GIM.GI_model, model[i, :], [0, 1], args=(self.params, dIt, self.Ib, self.u[i], self.Ra[i],
                                                                  self.HR[i], self.HRb, HRint, self.w))
            model[i + 1, :] = sol[1]

        self.model = pd.DataFrame(data=model, columns=['x1', 'x2', 'Ic', 'X', 'G', 'Y', 'Z'])


    def create_plot(self):
        if len(self.t_PA) == 0:
            self.plot = GIM.plot_glucose(self.model, self.sim_start)
        else:
            self.plot = GIM.plot_glucose(self.model, self.sim_start, pa=[self.PA_start, self.PA_start+self.PA_dur])


    def run_simulation(self, plot=True):
        # carry out full simulation
        self.create_input()
        self.get_parameters()
        if (self.adjusted == 2) or (self.adjusted == 3):
            self.determine_ins_cond()
            self.meal_bolus_reduction()
            self.CHO_during_PA()
        self.compute_model()
        if plot is True:
            self.create_plot()
