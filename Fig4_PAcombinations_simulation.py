# This script produces the data for Figure 4 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# Out:     simulated_data/Fig4_PAcombinations_BG/BG_scen[1,2,3]_adj[0,1,2,3]'.csv
# Date:    June 11, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import functions.Simulator as SIM
import pandas as pd


''' define scenario and adjustment strategy '''

adj = 0                         # adjustment  -->  0: no adjustment; 1: CHO algorithm
                                # 2: consensus guidelines (low CHO); 3: consensus guidelines (high CHO)

# Scenario 1: Exercise in near-fasted state
scen = 1
t_u = [0.75, 8.25, 12.75]       # time of meal bolus injections [h]
t_meal = [1, 8.5, 13]           # time of meal intake [h]
PA_start = 6                    # exercise start [h]

# Scenario 2: Exercise with insulin bolus reduction
# scen = 2
# t_u = [0.75, 6.75, 12.75]
# t_meal = [1, 7, 13]
# PA_start = 8.5

# Scenario 2: Exercise without insulin bolus reduction
# scen = 3
# t_u = [0.75, 6.75, 12.75]
# t_meal = [1, 7, 13]
# PA_start = 9.5


''' define intensities and durations of exercise sessions '''

intensity = [120, 140, 160]
duration = [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]


''' run simulations '''

Gt = 120                # target glucose [mg/dl]
Ib = 10                 # basal insulin [muU/ml]
ICR = 15                # insulin-to-carb ratio
CF = 20                 # correction factor
HRb = 80                # basal heart rate
w = 70                  # weight [kg]
dur = 24                # simulation duration [h]
start = 6               # simulation start time [h]

# run simulations for each intensity-duration combination and save in dataframe
BG = pd.DataFrame(data={})
for i in range(len(intensity)):
    for j in range(len(duration)):

        sim = SIM.Simulator(Gt, Ib, ICR, CF, HRb, w, dur, start, adjustment=adj)

        # define insulin input
        sim.t_u = t_u

        # define meal input
        sim.t_meal = t_meal
        sim.d_meal = [50, 70, 60]
        sim.tau_m = [60, 60, 60]

        # define PA input
        sim.t_PA = [PA_start, PA_start + duration[j]]
        sim.d_PA = [intensity[i]]

        sim.run_simulation(plot=False)

        BG[str(intensity[i])+', '+str(duration[j])] = sim.model['G']


''' save simulated glucose data '''

# BG.to_csv('simulated_data/Fig4_PAcombinations_BG/BG_scen' + str(scen) + '_adj' + str(adj) + '.csv',
#           sep=';', index=False)
