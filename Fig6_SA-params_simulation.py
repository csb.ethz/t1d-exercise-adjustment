# This script produces the data for Figure 6 of the following manuscript:
# Title:   Model-based evaluation of treatment adjustment to exercise in type 1 diabetes
# Authors: Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach*
# *Corresponding author:
#          michael.kaltenbach@bsse.ethz.ch
#
# Out:     simulated_data/Fig6_SA-params_output/[TIR,LBGI]_SAparams.csv
# Date:    June 11, 2021
# Author:  Julia Deichmann <julia.deichmann@bsse.ethz.ch>

import functions.Simulator as SIM
import functions.performance_measures as meas
from SALib.sample.saltelli import sample
import numpy as np


Gt = 120
Ib = 10
ICR = 15
CF = 20
HRb = 80
w = 70
dur = 24
start = 6


''' SALib problem definition and sampling '''

# import parameters
params_nom = np.genfromtxt('functions/model_parameters.csv').tolist()
param_names = ['p1', 'p2', 'p3', 'Gb', 'Vg', 'k21', 'kd', 'ka', 'ke', 'Vi', 'alpha', 'beta', 'tau_HR', 'tau', 'a', 'n']

# define parameter bounds
sd_factor = 0.2         # parameter range: +- 20%
bounds = []
for i in range(len(params_nom)):
    tmp = params_nom[i]
    tmp_b = [tmp - abs(sd_factor*tmp), tmp + abs(sd_factor*tmp)]
    bounds.append(tmp_b)

# define SALib problem
problem = {
    'num_vars': len(param_names),
    'names': param_names,
    'bounds': bounds
}

# sample parameter sets
n = 1500                                                    # number of samples  --> D = 51,000 parameter sets
params = sample(problem, n, calc_second_order=True)         # sample parameter sets
D = len(params)


''' run simulations and compute LBGI and HBGI '''

TIR = np.zeros(D)
LBGI = np.zeros(D)

for i in range(len(params)):
    print(i, '/', D)

    sim = SIM.Simulator(Gt, Ib, ICR, CF, HRb, w, dur, start, adjustment=0)

    # define time of insulin input
    sim.t_u = [0.75, 6.75, 12.75]

    # define meal input
    sim.t_meal = [1, 7, 13]
    sim.d_meal = [50, 70, 60]
    sim.tau_m = [60, 60, 60]

    # define PA input
    sim.t_PA = [9.5, 10.5]
    sim.d_PA = [120]

    sim.create_input()
    sim.params = params[i, :]                           # set model parameters
    if (sim.adjusted == 2) or (sim.adjusted == 3):
        sim.determine_ins_cond()
        sim.meal_bolus_reduction()
        sim.CHO_during_PA()
    sim.compute_model()

    G = sim.model['G']

    TIR[i] = meas.computeTIR(G)
    LBGI[i] = meas.computeLBGI(G)


''' save TIR and LBGI '''

# np.savetxt('simulated_data/Fig6_SA-params_output/TIR_SAparams.csv', TIR, delimiter=',')
# np.savetxt('simulated_data/Fig6_SA-params_output/LBGI_SAparams.csv', LBGI, delimiter=',')
