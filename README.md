### Information

This repository contains figure-generating scripts in Python for the manuscript:  

Julia Deichmann, Sara Bachmann, Marie-Anne Burckhardt, Gabor Szinnai, Hans-Michael Kaltenbach. **Simulation-based evaluation of treatment adjustment to exercise in type 1 diabetes**, 2021. Frontiers in Endocrinology.

\* Corresponding author: Hans-Michael Kaltenbach <michael.kaltenbach@bsse.ethz.ch>

### Requirements

* Python (3.7)
* Python packages: numpy (1.19.1), scipy (1.5.0), pandas (1.1.0), seaborn (0.11.1), matplotlib (3.2.2), salib (1.3.13)

### Author

Julia Deichmann <julia.deichmann@bsse.ethz.ch>

### License

Licensed under the MIT license (see LICENSE file)

